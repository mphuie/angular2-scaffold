/**
 * Created by mhuie on 9/15/16.
 */
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';

@Component({
  selector: 'blank-component',
  template:`
    <p>This is a blank component</p>
  `,
  styles: []
})

export class BlankComponent implements OnInit {
  ngOnInit(): void {
    this.getData();
  }
}