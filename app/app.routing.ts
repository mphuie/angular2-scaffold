import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestComponent } from './test-http.component';
import {HomeComponent} from "./home.component";

const appRoutes: Routes = [
  {
    path: 'test',
    component: TestComponent
  },
  {
    path: '',
    component: HomeComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);