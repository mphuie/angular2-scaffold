import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';

import { AppComponent } from './app.component';
// import { HeroDetailComponent } from './hero-detail.component';
import { TestComponent } from './test-http.component'
import {HomeComponent} from "./home.component";
@NgModule({
  imports:  [BrowserModule, HttpModule, routing],
  declarations: [TestComponent, AppComponent, HomeComponent],
  bootstrap: [AppComponent]
})

export class AppModule { }