import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Component({
  selector: 'test-component',
  template:`<p>hi from test component</p><li *ngFor="let hero of heroes">id: {{ hero.id }} - name: {{ hero.name }}</li>`,
  styles: [`li { color: green; }`]
})

export class TestComponent implements OnInit { 
  constructor(private http: Http) { }
  private heroes;

  getData(): void {
    this.http.get('http://www.mocky.io/v2/57db25850f0000400e8b6f8d')
      .map((res:Response) => res.json())
      .subscribe(
        data => { this.heroes = data; console.log(data); }
      )
  }

  ngOnInit(): void {
    this.getData();
  }

}