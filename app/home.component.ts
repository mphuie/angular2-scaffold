/**
 * Created by mhuie on 9/15/16.
 */
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';

@Component({
  selector: 'home-component',
  template:`
    <p>Home</p>
  `,
  styles: []
})

export class HomeComponent implements OnInit {
  ngOnInit(): void {
    this.getData();
  }
}